\chapter{Introduction}
\pagenumbering{arabic}

In algebraic topology, the cup product on reduced cohomology of a pointed space~\(X\) in degrees \(m\) and \(n\) with coefficients in abelian groups \(G\) and \(H\) is a map
\[{\cup}\quad:\quad
\redCoH{m}{X}{G} \times \redCoH{n}{X}{H} \to \redCoH{m+n}{X}{G \otimes H}.\]
In this master's thesis I construct a cup product map on cohomology in homotopy type theory, a logical framework in which it is possible to do synthetic homotopy theory. I will also prove that it is graded commutative.

In this setting, the reduced cohomology group of a \emph{pointed type}~\(X\) with coefficients in an abelian group \(G\) is defined as the set
\[\redCoH{m}{X}{G} \deq \trunc{X \pto \K{G}{m}}{0}\]
of pointed maps from \(X\) to \(\K{G}{m}\), the Eilenberg--MacLane space for \(G\) of dimension~\(m\), modulo homotopy. The cup product map will be induced by a pointed map
\[\K{G}{m} \times \K{H}{n} \pto \K{G \otimes H}{m+n}\]
on Eilenberg--MacLane spaces, whose construction occuppies a large part of this thesis.

In type theory, it is possible to directly start talking about homotopy theoretic concepts like paths, homotopies and homotopy equivalences. In contrast, when doing homotopy theory in set theory, one first has to define real numbers and topological spaces and later deal with the intricacies of point set topology.
It is this property of homotopy type theory that makes it an attractive target for computer formalization. Every lemma and every theorem that appears in this thesis has been first formalized in the Agda proof assistant.

This thesis is organized as follows:

\Cref{chap:basics} introduces type theory, its homotopy-theoretical interpretation and the univalence axiom. We also give various definitions and lemmas. Most of them are well-known and have appeared in the HoTT book or elsewhere. We included these nevertheless to establish some common names and notations.

In \Cref{chap:hits} we add three higher inductive types to our type theory. The first two, truncation and suspension, will be used to define \EilenbergMacLane{} spaces while the third, the smash product, will play a crucial role in the construction of the cup product.

In \Cref{chap:eilenberg-maclane} we review the construction of \EilenbergMacLane{} spaces by Licata and Finster and of cohomology in homotopy type theory. In \Cref{sec:generalized-induction} we then describe how to obtain generalized induction and recursion principles that will help us define the cup product in degrees \(m=n=1\).

\Cref{chap:two-semi-functors} is a technical chapter on the concept of 2-semifunctors. These capture exactly the data needed by the generalized recursion principle of \EilenbergMacLane{} spaces. The chapter also constructs several 2-semifunctors which will be used in the definition of the cup product map on \EilenbergMacLane{} spaces in low degrees.

In \Cref{chap:cp-construction} we define the cup product first on \EilenbergMacLane{} spaces and then use that to get the cup product on cohomology. There are several cases in the construction of the cup product on \EilenbergMacLane{} spaces: The case in which \(m=0\) or \(n=0\) is easy. In the case \(m=n=1\) we use the recursion principle from \Cref{sec:generalized-induction} and the theory of 2-semifunctors. Finally, we use the construction for \(m=n=1\) for the more general case in which \(m \geq 1\) and \(n \geq 1\). This requires defining a function \(\iterSuspSmashIterSuspOut{m}{n} : \iterSusp{m}{X} \wedge \iterSusp{m}{Y} \pto \iterSusp{m+n}{X \wedge Y}\), which roughly speaking takes the suspension from the inside of a smash product to the outside.

In \Cref{chap:commutativity} we prove graded commutativity of the cup product. This proof follows the construction of the cup product: The case \(m=0\) or \(n=0\) is easy. The case \(m=1\) and \(n=1\) uses the induction principles of \(\K{G}{1}\) and \(\K{H}{1}\). For \(m \geq 2\) and \(n \geq 1\) we investigate \(\iterSuspSmashIterSuspOut{m}{n}\) which turns out to be graded commutative in a certain sense. Finally we prove commutativity of the cup product on cohomology.

Finally \Cref{chap:formalization} discusses the formalization of the results in the Agda proof assistant.

I am thankful to several people for their support of this thesis project. First of all, I want to thank my advisor Marc Nieper-Wißkirchen for his guidance and for letting me take on this project. I am very grateful to Favonia for their support and encouragement of my formalization efforts in Agda. This text also benefited from careful eyes of Ingo Blechschmidt and Matthias Hutzler, who have pointed out many small mistakes.
